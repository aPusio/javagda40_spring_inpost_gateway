package com.sda.inpost.gateway;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RandomNumberController {

	private final SingletonShippingBoxNumberGenerator singletonShippingBoxNumberGenerator;
	private final RequestShippingBoxNumberGenerator requestShippingBoxNumberGenerator;

	@GetMapping("random")
	public int generateRandom(){
		return singletonShippingBoxNumberGenerator
				   .getRandomNumber();
	}

	@GetMapping("requestrandom")
	public int generateRequestRandom(){
		return requestShippingBoxNumberGenerator
			.getRandomNumber();
	}
}
