package com.sda.inpost.gateway.profilebeans;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class HelloController {

	private final SayHello sayHello;

	@GetMapping("sayhello")
	public String sayHello(){
		return sayHello.hello();
	}
}
