package com.sda.inpost.gateway.profilebeans;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class HelloFromTest implements SayHello{
	@Override
	public String hello() {
		return "hello from test component!";
	}
}
