package com.sda.inpost.gateway;

import java.util.Random;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
public class RequestShippingBoxNumberGenerator {
	private int randomNumber;

	public RequestShippingBoxNumberGenerator() {
		Random random = new Random();
		randomNumber = random.nextInt(9);
	}

	public int getRandomNumber() {
		return randomNumber;
	}
}
