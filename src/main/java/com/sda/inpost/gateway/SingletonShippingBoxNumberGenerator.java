package com.sda.inpost.gateway;

import java.util.Random;

import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class SingletonShippingBoxNumberGenerator {
	private int randomNumber;

	public SingletonShippingBoxNumberGenerator() {
		Random random = new Random();
		randomNumber =  random.nextInt(9);
	}

	public int getRandomNumber() {
		return randomNumber;
	}
}
